package com.example.practica;

import java.util.ArrayList;

public class Resurse {





    public String getFresch(Boolean isFreshs) {
        String isFreshString;
        if(isFreshs){
            isFreshString = "свежий" ;
        }else {
            isFreshString = "не свежий";
        }
        return isFreshString;
    }

    public String getCoocket(Boolean withSugar, Boolean withPoppy, Boolean withSesame) {

        ArrayList<String> items = new ArrayList<>();
        if (withSugar)
            items.add("с сахаром");
        if (withPoppy)
            items.add("с маком");
        if (withSesame)
            items.add("с кунжутом");

        return String.format("Булочка %s", String.join(", ", items));
    }



    public String getChockolate(String type) {
        String typeString = "";
        switch (type)
        {
            case "white":
                typeString = "белый";
                break;
            case "black":
                typeString = "черный";
                break;
            case "milk":
                typeString = "молочный";
                break;
        }

        return String.format("%s", typeString);
    }
}