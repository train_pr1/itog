package com.example.practica;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;




public class Operator implements  Initializable{
    @FXML
    private ImageView fotoUser;
    @FXML
    private Label welcome;
    @FXML
    public Button exit;




    @Override
    public void initialize(URL location, ResourceBundle resourceBundle) {
       welcome.setText("Вы вошли как: "+Avtorization.NameFooter);

    }
    public void exitAccaunt(ActionEvent actionEvent) {
        Stage primaryStage = new Stage();
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("avtorization.fxml"));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        primaryStage.setTitle("Магазин");
        primaryStage.getIcons().add(new Image("free_icon.png"));
        primaryStage.setScene(new Scene(root, 892, 400));
        primaryStage.show();
        Stage stage = (Stage) exit.getScene().getWindow();
        stage.close();

    }


}
