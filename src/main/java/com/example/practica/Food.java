package com.example.practica;

public class Food
{
    public String kkal;// количество калорий
    public String title;// название
    public String opisanie;

    public Food(String kkal, String title, String opisanie) {
        this.setKkal(kkal);
        this.setTitle(title);
        this.setOpisanie(opisanie);
    }


    @Override
    public String toString() {
        return String.format("%s: %s ккал : %s", this.getTitle(), this.getKkal(), this.getOpisanie());
    }

    public String getKkal() {
        return kkal;
    }

    public void setKkal(String kkal) {
        this.kkal = kkal;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return "";
    }

    public void setOpisanie(String opisanie) {
        this.opisanie = opisanie;
    }

    public String getOpisanie() {
        return opisanie;
    }



}