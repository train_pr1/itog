package com.example.practica;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;

public class FoodFormController implements Initializable {


    // создаем
    public ChoiceBox cmbFoodType;
    public TextField txtFoodTitle;
    public TextField txtFoodKkal;

    public VBox fruitPane;
    public CheckBox chkIsFresh;

    public HBox chocolatePane;
    public ChoiceBox cmbChocolateType;

    public VBox cookiePane;
    public CheckBox chkWithSugar;
    public CheckBox chkWithPoppy;
    public CheckBox chkWithSesame;

    public Label error;

    final String FOOD_FRUIT = "Фрукт";
    final String FOOD_CHOCOLATE = "Шоколадка";
    final String FOOD_COOKIE = "Булочка";

    // добавляем новое поле
    private Boolean modalResult = false;

    MainFormController helloController = new MainFormController();
    Cookie cookie;
    Chocolate chocolate;
    Resurse resurse = new Resurse();

    DB db = new DB();


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        cmbFoodType.setItems(FXCollections.observableArrayList(
                FOOD_FRUIT,
                FOOD_CHOCOLATE,
                FOOD_COOKIE
        ));

        cmbFoodType.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
// вызываю новую функцию
            updatePanes((String) newValue);
        });
// добавляем все три типа шоколада в комобобокс
        cmbChocolateType.getItems().setAll(
                Chocolate.Type.white,
                Chocolate.Type.black,
                Chocolate.Type.milk
        );


        updatePanes("");

    }


    // добавил новую функцию
    public void updatePanes(String value) {
        this.fruitPane.setVisible(value.equals(FOOD_FRUIT));
        this.fruitPane.setManaged(value.equals(FOOD_FRUIT));
        this.chocolatePane.setVisible(value.equals(FOOD_CHOCOLATE));
        this.chocolatePane.setManaged(value.equals(FOOD_CHOCOLATE));
        this.cookiePane.setVisible(value.equals(FOOD_COOKIE));
        this.cookiePane.setManaged(value.equals(FOOD_COOKIE));
    }

    // обработчик нажатия на кнопку Сохранить
    public void onSaveClick(ActionEvent actionEvent) {
        this.modalResult = true; // ставим результат модального окна на true
// закрываем окно к которому привязана кнопка
        ((Stage) ((Node) actionEvent.getSource()).getScene().getWindow()).close();
    }

    public void onCancelClick(ActionEvent actionEvent) {
        this.modalResult = false; // ставим результат модального окна на false
// закрываем окно к которому привязана кнопка
        ((Stage) ((Node) actionEvent.getSource()).getScene().getWindow()).close();
    }

    // геттер для результата модального окна
    public Boolean getModalResult() {
        return modalResult;
    }

    public void setFood(Food food) throws SQLException, ClassNotFoundException {
// делаем так что если объект редактируется, то нельзя переключать тип
        this.cmbFoodType.setDisable(food != null);
        if (food != null) {
// ну а тут стандартное заполнение полей в соответствии с переданной едой
            this.txtFoodKkal.setText(String.valueOf(food.getKkal()));
            this.txtFoodTitle.setText(food.getTitle());
            int index = helloController.index;
            String string = db.getType(index);
            if (string.equals("Фрукты")) { // если фрукт
                this.cmbFoodType.setValue(FOOD_FRUIT);
            } else if (string.equals("Булочка")) { // если булочка
                this.cmbFoodType.setValue(FOOD_COOKIE);

            } else if (string.equals("Шоколад")) { // если шоколад
                this.cmbFoodType.setValue(FOOD_CHOCOLATE);

            }
        }
    }

    public Food getFood() {
        Food result = null;
        String kkal = this.txtFoodKkal.getText();
        String title = this.txtFoodTitle.getText();

        System.out.println();
        switch ((String)this.cmbFoodType.getValue()) {
            case FOOD_CHOCOLATE:
                String chocolateStr = resurse.getChockolate(String.valueOf(this.cmbChocolateType.getValue()));
                result = new Chocolate(kkal, title, chocolateStr );
                System.out.println(result);
                break;
            case FOOD_COOKIE:
                String cookiesStr = resurse.getCoocket(
                        this.chkWithSugar.isSelected(),
                        this.chkWithPoppy.isSelected(),
                        this.chkWithSesame.isSelected()
                );
                result = new Cookie(
                        kkal,
                        title,
                        cookiesStr
                );
                break;
            case FOOD_FRUIT:
                String fruitStr = resurse.getFresch(this.chkIsFresh.isSelected());
                System.out.println(fruitStr);
                result = new Fruit(kkal, title, fruitStr );
                break;
        }
        return result;
    }



}



