package com.example.practica;

import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Optional;
import java.util.ResourceBundle;

public class MainFormController implements Initializable {
    public TableView mainTable;

    @FXML
    private Button exit;
    @FXML
    private Label welcome;


    // создали список
    ObservableList<Food> foodList = FXCollections.observableArrayList();
    DB db = new DB();

    public static int index;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        welcome.setText("Добро пожаловать вы вошли как: "+ Avtorization.NameFooter);
// заполнили список данными
        ArrayList<String> product = new ArrayList<>();

        try {
            product = db.getList();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        System.out.println(product);
        for(int i = 0; i < product.size(); i++){
            foodList.add(new Food(product.get(i), product.get(++i), product.get(++i)));
        }
// foodList.add(new Fruit("100", "Яблоко", true));
// foodList.add(new Chocolate("200", "шоколад Аленка", Chocolate.Type.milk));
// foodList.add(new Cookie("300", "сладкая булочка с Маком", true, true, false));

// подключили к таблице

        mainTable.setItems(foodList);

// создаем столбец, указываем что столбец преобразует Food в String,
// указываем заголовок колонки как "Название"
        TableColumn<Food, String> titleColumn = new TableColumn<>("Название");
// указываем какое поле брать у модели Food
// в данном случае title, кстати именно в этих целях необходимо было создать getter и setter для поля title
        titleColumn.setCellValueFactory(new PropertyValueFactory<>("title"));

// тоже самое для калорийности
        TableColumn<Food, String> kkalColumn = new TableColumn<>("Калорийность");
        kkalColumn.setCellValueFactory(new PropertyValueFactory<>("kkal"));

        TableColumn<Food, String> opisanieColumn = new TableColumn<>("Описание");
        opisanieColumn.setCellValueFactory(new PropertyValueFactory<>("opisanie"));


// подцепляем столбцы к таблице
//mainTable.getColumns().addAll(titleColumn, kkalColumn);


// добавляем столбец с описанием
        TableColumn<Food, String> descriptionColumn = new TableColumn<>("Описание");
// если хотим что-то более хитрое выводить, то используем лямбда выражение
        descriptionColumn.setCellValueFactory(cellData -> {
// плюс надо обернуть возвращаемое значение в обертку свойство
            return new SimpleStringProperty(cellData.getValue().getDescription());
        });

// добавляем сюда descriptionColumn
        mainTable.getColumns().addAll(titleColumn, kkalColumn, opisanieColumn);


    }
    // добавляем инфу что наш код может выбросить ошибку IOException
    public void onAddClick(ActionEvent actionEvent) throws IOException {
// эти три строчки создюат форму из fxml файлика
// в принципе можно было бы обойтись
// Parent root = FXMLLoader.load(getClass().getResource("FoodForm.fxml"));
// но дальше вот это разбиение на три строки упростит нам жизнь
        FXMLLoader loader = new FXMLLoader(MainFormController.class.getResource("FoodForm.fxml"));

        Parent root = loader.load();

// ну а тут создаем новое окно
        Stage stage = new Stage();
        stage.getIcons().add(new Image("free_icon.png"));
        stage.setScene(new Scene(root));
// указываем что оно модальное
        stage.initModality(Modality.WINDOW_MODAL);
// указываем что оно должно блокировать главное окно
// ну если точнее, то окно, на котором мы нажали
        //на кнопку
        stage.initOwner(this.mainTable.getScene().getWindow());

// открываем окно и ждем пока его закроют
        stage.showAndWait();

// вытаскиваем контроллер который привязан к форме
        com.example.practica.FoodFormController controller = loader.getController();
// проверяем что наали кнопку save
        if (controller.getModalResult()) {
// собираем еду с формы
            Food newFood = controller.getFood();
// добавляем в список
            this.foodList.add(newFood);
        }
    }

    public void onEditClick(ActionEvent actionEvent) throws IOException, SQLException, ClassNotFoundException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("FoodForm.fxml"));
        Parent root = loader.load();
        index = this.mainTable.getSelectionModel().getSelectedIndex()+1;
        Stage stage = new Stage();
        stage.getIcons().add(new Image("free_icon.png"));
        stage.setScene(new Scene(root));
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initOwner(this.mainTable.getScene().getWindow());


// передаем выбранную еду
        com.example.practica.FoodFormController controller = loader.getController();
        controller.setFood((Food) this.mainTable.getSelectionModel().getSelectedItem());

        stage.showAndWait();

// если нажали кнопку сохранить
        if (controller.getModalResult()) {
// узнаем индекс выбранной в таблице строки
// подменяем строку в таблице данными на форме
            this.mainTable.getItems().set(index-1, controller.getFood());
        }
    }


    public void onDeleteClick(ActionEvent actionEvent) {
// берем выбранную на форме еду
        Food food = (Food) this.mainTable.getSelectionModel().getSelectedItem();

// выдаем подтверждающее сообщение
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Подтверждение");
        alert.setHeaderText(String.format("Точно удалить %s?", food.getTitle()));

// если пользователь нажал OK
        Optional<ButtonType> option = alert.showAndWait();
        if (option.get() == ButtonType.OK) {
// удаляем строку из таблицы
            this.mainTable.getItems().remove(food);
        }
    }
    public void exitAccaunt(ActionEvent actionEvent) {
        Stage primaryStage = new Stage();
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("avtorization.fxml"));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        primaryStage.setScene(new Scene(root, 600, 400));
        primaryStage.getIcons().add(new Image("free_icon.png"));
        primaryStage.setMaxHeight(500);
        primaryStage.setMinHeight(400);
        primaryStage.setMaxWidth(700);
        primaryStage.setMinWidth(600);
        primaryStage.setTitle("Магазин");
        primaryStage.show();
        Stage stage = (Stage) exit.getScene().getWindow();
        stage.close();

    }


}