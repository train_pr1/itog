package com.example.practica;

import java.sql.*;
import java.util.ArrayList;
import java.util.Map;

public class DB {

    private Connection dbConn = null;

    private Connection getDBConn() throws ClassNotFoundException, SQLException {
        Map<String, String> env = System.getenv();
        String host = env.getOrDefault("DB_HOST", "127.0.0.1");
        String port = env.getOrDefault("DB_PORT", "3306");
        String database = env.getOrDefault("DB_NAME", "pr_2");
        String user = env.getOrDefault("DB_USER", "root");
        String password = env.getOrDefault("DB_HOST", "");

        String connectionURL = String.format("jdbc:mysql://%s:%s/%s?serverTimezone=UTC",

                host,
                port,
                database);

        Class.forName("com.mysql.cj.jdbc.Driver");

        if (dbConn == null){
            dbConn = DriverManager.getConnection(
                    connectionURL,
                    user,
                    password
            );
        }

        return dbConn;
    }

    public Integer getSotrydnic(String number) throws SQLException, ClassNotFoundException {
        String sql ="SELECT count(*) FROM sotrudniki WHERE telephon='"+number+"';";
        Statement statement = getDBConn().createStatement();
        ResultSet result = statement.executeQuery(sql);
        Integer vxod = 0;
        while (result.next()){
            vxod = result.getInt(1);
        }
        return vxod;
    }

    public String getPassword(String number) throws SQLException, ClassNotFoundException {
        String sql ="SELECT sotrudniki.password FROM sotrudniki where telephon='"+number+"'";
        Statement statement = getDBConn().createStatement();
        ResultSet result = statement.executeQuery(sql);
        String vxod = "";
        while (result.next()){
            vxod = result.getString(1);
        }
        return vxod;
    }

    public String getRole(String number) throws SQLException, ClassNotFoundException {
        String sql ="SELECT role.role FROM pr_2.sotrudniki, pr_2.role where role.id_role = sotrudniki.id_role and sotrudniki.telephon = '"+number+"'";
        Statement statement = getDBConn().createStatement();
        ResultSet result = statement.executeQuery(sql);
        String vxod = "";
        while (result.next()){
            vxod = result.getString(1);
        }
        return vxod;
    }




    public ArrayList<String> getList() throws SQLException, ClassNotFoundException {
        String sql ="SELECT kolorii, food.food_name, food_types.food_type_name FROM food, food_types where food.food_types_idfood_types = food_types.idfood_types;";
        Statement statement = getDBConn().createStatement();
        ResultSet result = statement.executeQuery(sql);
        ArrayList<String> product = new ArrayList<>();
        while (result.next()){
            product.add(result.getString(1));
            product.add(result.getString(2));
            product.add(result.getString(3));
        }
        return product;
    }

    public ArrayList<String> getListProduct() throws SQLException, ClassNotFoundException {
        String sql ="SELECT distinct(food_type_name) FROM food_types;";
        Statement statement = getDBConn().createStatement();
        ResultSet result = statement.executeQuery(sql);
        ArrayList<String> product = new ArrayList<>();
        while (result.next()){
            product.add(result.getString(1));
        }
        return product;
    }

    public String getType(int i) throws SQLException, ClassNotFoundException {
        String sql ="SELECT food_types.food_type_name FROM food, food_types where food_types.idfood_types=food.food_types_idfood_types and food.idfood='"+i+"';";
        Statement statement = getDBConn().createStatement();
        ResultSet result = statement.executeQuery(sql);
        String product = "";
        while (result.next()){
            product = result.getString(1);
        }
        System.out.println(product);
        return product;
    }

}